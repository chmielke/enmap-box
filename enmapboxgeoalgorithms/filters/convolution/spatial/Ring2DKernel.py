from astropy.convolution import Ring2DKernel

kernel = Ring2DKernel(radius_in=3, width=2)
