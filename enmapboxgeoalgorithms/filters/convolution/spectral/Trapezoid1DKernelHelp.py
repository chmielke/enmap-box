from enmapboxgeoalgorithms.provider import Help, Link

helpAlg = Help(text='Applies Trapezoid1DKernel.')

helpCode = Help(text='Python code. See {} for information on different parameters.',
                links=[Link('http://docs.astropy.org/en/stable/api/astropy.convolution.Trapezoid1DKernel.html',
                            'astropy.convolution.Trapezoid1DKernel')])

