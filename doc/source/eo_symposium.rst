EnMAP-Box Schulung
##################

.. |link_sym| raw:: html

   <a href="https://www.dialogplattform-erdbeobachtung.de/cms" target="_blank">2. Symposium "Neue Perspektiven der Erdbeobachtung"</a>


Am 13.11.2019 findet im Rahmen des |link_sym| eine Kurzschulung zur
Arbeit mit der EnMAP-Box 3 statt. Die EnMAP-Box 3 ist ein frei-verfügbares QGIS Plugin zur Arbeit mit hyperspektralen
EnMAP Daten und wird als Teil der Nutzungsvorbereitungen für die EnMAP-Mission entwickelt. Die Teilnehmenden erhalten
einen Überblick über die Funktionalität des QGIS Plugins und die darin enthaltenen Applikationen. Am Beispiel einer
Regressionsanalyse wird die Nutzer*innen-freundliche Integration von Verfahren des Maschinellen Lernens vorgestellt
und live erprobt. Anschließend wird eine Einführung in die Arbeit mit dem ImageMath Tool der EnMAP-Box gegeben.

.. admonition:: Registrierung

   **Interessent*innen werden dringend um Registrierung unter** enmapbox@enmap.org **gebeten.**

Die Kurzschulung ist als „Hands-on Exercise“ unter Nutzung eines eigenen Laptops mit QGIS und EnMAP-Box Plugin geplant.
Teilnehmende werden daher gebeten, ein eigenes Laptop mitzubringen, auf dem QGIS und die EnMAP-Box
entsprechend :ref:`dieser Anleitung <usr_installation>` installiert sind. Nach erfolgreicher Registrierung erhalten sie zudem
Informationen zum Download der Testdaten.




