..  EnMAP-Box 3 documentation master file, created by
    sphinx-quickstart on Fri Jan 19 05:59:30 2018.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.


#########################
EnMAP-Box 3 Documentation
#########################

The EnMAP-Box is a python plugin for `QGIS <https://www.qgis.org/en/site/#>`_, designed to process and visualise
hyperspectral remote sensing data.

Get going with the :ref:`Installation <usr_installation>` and the :ref:`Getting Started <getting_started>` chapter.
Have a look at the :ref:`Cookbook <cookbook>` for usage examples!

Key features
************

* Extend your QGIS for remote sensing image analysis
* Add powerful tools to process and analyse imaging spectroscopy data
* Integrate machine learning algorithms into your image classification and regression with Random Forests, Support Vector Machines and many more
* Create and manage spectral libraries with attribute data
* Develop your own image processing algorithms using a powerful Python API


.. figure:: img/screenshot_main3.png
   :width: 95%
   :align: center


Related websites
****************

- `Environmental Mapping and Analysis Program (EnMAP) <http://www.enmap.org/>`_
- `Source code repository <https://bitbucket.org/hu-geomatics/enmap-box/src/develop/>`_
- `HYPERedu on eo-college <https://eo-college.org/now-online-hyperedu-educational-resources-on-imaging-spectroscopy/>`_


News
****

FOSSGIS 2020
============

Andreas Rabe presented the EnMAP-Box at the FOSSGIS 2020 in Freiburg. See the full live-demo session here (german):

.. raw:: html

   <iframe width="100%" height="380" src="https://www.youtube.com/embed/egaJLUe_eXY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|

QGIS User Conference 2019
=========================

Two Presentations by EnMAP-Box developers Andreas Rabe and Benjamin Jakimow at the QGIS User conference in Coruña

* `Earth observation data processing in QGIS with a python API <https://av.tib.eu/media/40775>`_
* `EO Time Series Viewer - A plugin to explore Earth Observation Time Series Data in QGIS <https://av.tib.eu/media/40776>`_

|
|


.. toctree::
    :maxdepth: 2
    :caption: General
    :hidden:

    general/about.rst
    general/faq_trouble.rst
    general/contribute.rst

..  toctree::
    :maxdepth: 2
    :caption: User Section
    :hidden:

    usr_section/usr_installation.rst
    usr_section/usr_gettingstarted.rst
    usr_section/usr_cookbook/usr_cookbook.rst
    usr_section/usr_manual/usr_manual.rst
    usr_section/application_tutorials/index.rst

..  toctree::
    :maxdepth: 2
    :caption: Developer Section
    :hidden:

    dev_section/dev_installation.rst
    dev_section/dev_enmapboxrepository
    dev_section/dev_publish_enmapbox.rst
    dev_section/dev_cookbook/dev_cookbook.rst
    dev_section/dev_guide.rst
    dev_section/api/modules.rst
    dev_section/programming_tutorials/index.rst



Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

